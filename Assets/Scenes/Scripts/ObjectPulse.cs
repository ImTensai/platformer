﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPulse : MonoBehaviour
{
    public float duration;

    void Start()
    {
        StartCoroutine(Pulse());
    }

    IEnumerator Pulse()
    {
        Vector3 startPos = transform.position;
        Vector3 endPos = transform.position + new Vector3 (0, 1.25f, 0);

        while (enabled)
        {

            for (float t = 0; t < duration; t += Time.deltaTime)
            {
                float frac = (t / duration);
                frac = frac * frac / (2 * frac * frac - 2 * frac + 1); //Formula to ramp up the movement speed over time
                transform.position = Vector3.Lerp(startPos, endPos, frac);
                yield return new WaitForEndOfFrame();
            }

            for (float t = 0; t < duration; t += Time.deltaTime)
            {
                float frac = (t / duration);
                frac = frac * frac / (2 * frac * frac - 2 * frac + 1);
                transform.position = Vector3.Lerp(endPos, startPos, frac);
                yield return new WaitForEndOfFrame();
            }
        }
        yield return new WaitForEndOfFrame();
    }
}
