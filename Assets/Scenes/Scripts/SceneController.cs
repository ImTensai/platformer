﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SceneController : MonoBehaviour
{
    public static SceneController sceneController;

    private GameObject lastSelection;

    public Text[] texts;
    //public Color startColor;

    private void Awake()
    {
        if(sceneController == null)
        {
            sceneController = this;
            DontDestroyOnLoad(sceneController);
        }
        else
        {
            Destroy(sceneController);
        }
        Cursor.visible = false;
    }

    void Start()
    {
        Scene activeScene = SceneManager.GetActiveScene();

        if(activeScene.name.Equals("Credits"))
        {
            //yield return new WaitForSeconds(2);
            //LoadStartMenu();
            //Debug.Log("Title Scene Loaded");
            //yield return new WaitForEndOfFrame();
            StartCoroutine(CreditScene());
        }
    }

    
    public void SeclectionChanged()
    {
        GameObject g = EventSystem.current.currentSelectedGameObject;
        lastSelection = g;
    }

    public static void LoadStartMenu()
    {
        SceneManager.LoadScene("TitleScreen");
    }

    public void NewGame()
    {
        SavePointController.DeleteSave();
        SceneManager.LoadScene("Platformer");
    }

    public void LoadGame()
    {
        SavePointController.SaveState saveState = SavePointController.Load();
        if (saveState == null)
        {
            Debug.Log("No Save file found. Starting new game.");
            NewGame();
        }
        else
        {
            Debug.Log("Save file found loading game"); Scene currentScene = SceneManager.GetActiveScene();
            if (currentScene.name != saveState.scene)
            {
                GameManager.LoadSaveFile(saveState);
            }
            else
            {
                PlayerController.player.dead = false;
                PlayerController.player.transform.position = saveState.position;
            }
        }
    }

    IEnumerator CreditScene()
    {
        yield return new WaitForSeconds(2);
        LoadStartMenu();
        Debug.Log("title scene loaded");
        yield return new WaitForEndOfFrame();

        //float fadeTime = 2;
        //for (float i = 0; i < 1; i += Time.deltaTime)
        //{
        //    image.color = Color.Lerp(Color.black, new Color(0, 0, 0, 0), (i / fadeTime));
        //    yield return new WaitForSeconds(2);
        //    Debug.Log("Credit scene done");
        //}

        //for (float i = 0; i < fadeTime; i += Time.deltaTime)
        //{
        //    image.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, (i / fadeTime));
        //    LoadStartMenu();
        //    image.color = Color.Lerp(Color.black, new Color(0, 0, 0, 0), (i / fadeTime));
        //    Debug.Log("Start menu");
        //    yield return new WaitForEndOfFrame();
        //}
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            ExitGame();
        }
    }
    void ExitGame()
    {
        Application.Quit();
    }
}

