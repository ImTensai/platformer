﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager game;
    public GameObject playerPrefab;
    public Image fadeImage;

    public float fadeDuration = 0.5f;
    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(game);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        fadeImage.enabled = false;
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "BossLevel")
        {
            AudioManager.PlayBossMusic();
        }
        else
        {
            AudioManager.PlayMainMusic();
        }
    }
    public static void LoadScene(string scene, int doorID)
    {
        game.StartCoroutine(game.LoadSceneCoroutine(scene, doorID));
    }

    IEnumerator LoadSceneCoroutine(string scene, int doorID)
    {
        Time.timeScale = 0;
        fadeImage.enabled = true;
        //fade out of scene
        for (float t = 0; t < fadeDuration; t += Time.unscaledDeltaTime)
        {
            float frac = t / fadeDuration;
            fadeImage.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, frac);
            yield return new WaitForEndOfFrame(); 
        }

        yield return SceneManager.LoadSceneAsync(scene);
        DoorController[] doors = GameObject.FindObjectsOfType<DoorController>();
        foreach(DoorController door in doors)
        {
            if (doorID == door.doorID)
            {
                PlayerController.player.transform.position = door.transform.position;
                break;
            }
        }
        
        // fade in scene
        for (float i = 0; i < fadeDuration; i += Time.unscaledDeltaTime)
        {
            float frac = i / fadeDuration;
            fadeImage.color = Color.Lerp(Color.black, new Color(0, 0, 0, 0), frac);
            yield return new WaitForEndOfFrame();
        }
        fadeImage.enabled = false;
        Time.timeScale = 1;
    }

    public static void LoadSaveFile(SavePointController.SaveState save)
    {
        game.StartCoroutine(game.LoadSaveFileCoroutine(save));
    }

    public IEnumerator LoadSaveFileCoroutine(SavePointController.SaveState saveState)
    {
        Debug.Log("Loading");
        yield return SceneManager.LoadSceneAsync(saveState.scene);
        if(PlayerController.player == null)
        {
            Instantiate(playerPrefab);
        }
        PlayerController.player.transform.position = saveState.position;
    }
}
