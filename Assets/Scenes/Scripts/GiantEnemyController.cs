﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantEnemyController : MonoBehaviour
{
    public static Animator anim;

    public float speed = 2;
    public float travelDistance = 4;

    public bool playerSeen;

    Vector3 startPos;
    Vector3 endPos;
    private void Awake()
    {
        startPos = transform.position;
        endPos = startPos + transform.right * travelDistance;
    }
    private void Start()
    {
        anim = GetComponent<Animator>();
        Physics2D.queriesStartInColliders = false;
        anim.Play("2DenemyIdle");
    }
    private void Update()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.right, 13.5f);
        if(hitInfo.collider != null)
        {
            if(hitInfo.collider.GetComponent<PlayerController>())
            {
                Debug.DrawLine(transform.position, hitInfo.point, Color.red);
                playerSeen = true;
                //Debug.Log("I see u");
            }
        }
        else
        {
            Debug.DrawLine(transform.position, transform.position + transform.right * 13.5f, Color.green);
        }

        if(playerSeen)//Chasing player
        {
            if(transform.position.x < endPos.x)
            {
                anim.Play("2DenemyWalking");
                transform.position += (Vector3.right * speed) * Time.deltaTime;
                //Debug.Log("Chasing");
            }
        }
        else //Resetting enemy
        {
            if(transform.position.x > startPos.x)
            {
                anim.Play("2DenemyWalking");
                transform.position -= (Vector3.right * speed) * Time.deltaTime;
                //Debug.Log("moving back");
            }
        }

        if(transform.position.x >= endPos.x && playerSeen == true)
        {
            playerSeen = false;
        }

        if (transform.position == startPos) //Enemy is at start position
        {
            playerSeen = false;
            transform.position = startPos;
            anim.Play("2DenemyIdle");
        }

        if (PlayerController.player.dead) //Player died
        {
            playerSeen = false;
            transform.position = startPos;
            anim.Play("2DenemyIdle");
        }
    }
    //IEnumerator ChasePlayer()
    //{
    //    anim.Play("2DenemyWalking");
    //    for (float i = 0; i <= 10; i += Time.deltaTime)
    //    {
    //        transform.position += (Vector3.right * speed) * Time.deltaTime;
    //        yield return new WaitForEndOfFrame();
    //    }
    //    for (float i = 0; i <= 10; i += Time.deltaTime)
    //    {
    //        transform.position += (Vector3.down * speed) * Time.deltaTime;
    //        yield return new WaitForEndOfFrame();
    //    }
    //    transform.position = startPos;
    //    yield return new WaitForEndOfFrame();
    //}
}
