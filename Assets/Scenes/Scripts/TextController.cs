﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public static TextController instance;

    public GameObject textBox;
    public GameObject icon;
    public GameObject yesNoBox;

    Transform yesOption;
    Transform noOption;
    Transform yesNoSelector;

    public delegate void YesNoCallback(bool answer);

    public Text text;
    
    public float delay = 0.1f;

    Queue<IEnumerator> textQueue = new Queue<IEnumerator>();

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    void Setup()
    {
        textBox.SetActive(true);
        //Time.timeScale = 0;
    }

    void CleanUp()
    {
        textBox.SetActive(false);
        Time.timeScale = 1;
        //icon.SetActive(false);
    }

    private IEnumerator Start()
    {
        textBox.SetActive(false);
        while (enabled)
        {
            if (textQueue.Count > 0)
            {
                Setup();
                while (textQueue.Count > 0)
                {
                    yield return StartCoroutine(textQueue.Dequeue());
                }
                CleanUp();
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public void RevealText(string text, float delay, float duration)
    {
        instance.textQueue.Enqueue(instance.RevealTextCoroutine(text,delay,duration));
    }

    IEnumerator RevealTextCoroutine(string text, float delay, float duration)
    {
        this.text.text = "";

        for (int i = 0; i <= text.Length; i++)
        {
            this.text.text = text.Substring(0, i);
            yield return new WaitForSeconds(delay);
        }
        yield return new WaitForSecondsRealtime(duration);
    }
}
