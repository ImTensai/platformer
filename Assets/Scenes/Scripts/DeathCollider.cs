﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathCollider : MonoBehaviour
{
    GameOverScript gameOver;
    private void Start()
    {
        GameOverScript.gameOver.gameOverTextBox.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player"))
        {
            Debug.Log("HERE");
            PlayerController.player.dead = true;
            AudioManager.Play("DeathSound");
            StartCoroutine(ResetPlayerCoroutine());
        }
    }
    IEnumerator ResetPlayerCoroutine()
    {
        SavePointController.SaveState save = SavePointController.Load();
        GameOverScript.gameOver.gameOverTextBox.SetActive(true);
        GameOverScript.gameOver.gameOverText.text = "Imbecile";
        yield return new WaitForSeconds(2);

        if(PlayerController.player.dead == true && SavePointController.SaveFileExists())
        {
            Scene currentScene = SceneManager.GetActiveScene();
            if(currentScene.name != save.scene)
            {
                GameManager.LoadSaveFile(save);
            }
            PlayerController.player.dead = false;
            PlayerController.player.transform.position = save.position;
        }
        else if(PlayerController.player.dead == true)
        {
            PlayerController.player.dead = false;
            PlayerController.player.transform.position = new Vector3(0, 0, 0);
        }

        GameOverScript.gameOver.gameOverTextBox.SetActive(false);
        TextController.instance.text.text = string.Empty;
        yield return new WaitForEndOfFrame();
    }
}
