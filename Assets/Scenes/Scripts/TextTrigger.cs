﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTrigger : MonoBehaviour
{
    public string displaytext = "";
    public float duration = 2;

    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.CompareTag("Player"))
        {
            TextController.instance.RevealText(displaytext, 0.05f, duration);
        }
    }

}
