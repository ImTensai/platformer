﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : InteractableController
{
    public string scene;
    public int doorID;
    public int destinationID;
    public override void Interact()
    {
        GameManager.LoadScene(scene, destinationID);
    } 
}
