﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CameraController : MonoBehaviour
{
    public static CameraController cam;
    public float speed = 0.25f;
    public Transform target;
    public Vector3 offset = new Vector3(2, 0, 0);

    bool bossRoom;
    public Vector3 bossRoomOffset = new Vector3(3, 1, 0);

    Animator anim;

    private void Awake()
    {
        if (cam == null)
        {
            cam = this;
        }
        else
        {
            cam.GetComponent<Camera>().backgroundColor = GetComponent<Camera>().backgroundColor;
            Destroy(cam);
        }
        
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        Scene activeScene = SceneManager.GetActiveScene();

        if (activeScene.name.Equals("BossLevel"))
        {
            bossRoom = true;
        }
    }

    void FixedUpdate() //is always keeping its offset once the target is found
    {
        if (target == null) return;

        if (bossRoom == false)
        {
            if (target.transform.rotation.y == 0)
            {
                transform.position = Vector3.Lerp(transform.position, target.position + offset, Time.fixedDeltaTime * speed);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, target.position + new Vector3(-offset.x, offset.y, offset.z), Time.fixedDeltaTime * speed);
            }
        }

        else
        {
            if (target.transform.rotation.y == 0)
            {
                transform.position = Vector3.Lerp(transform.position, target.position + bossRoomOffset, Time.fixedDeltaTime * speed);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, target.position + new Vector3(-bossRoomOffset.x, bossRoomOffset.y, bossRoomOffset.z), Time.fixedDeltaTime * speed);
            }
        }
        
    }

    public static void Shake()
    {
        cam.anim.SetTrigger("cameraShake");
    }

    private void Update() //is always finding the targets position
    {
        if(Application.isPlaying && target == null)
        {
            target = PlayerController.player.transform;
            if (target) transform.position = target.position + offset;
        }

        if(Application.isPlaying && target == null && bossRoom == true)
        {
            target = PlayerController.player.transform;
            if (target) transform.position = target.position + bossRoomOffset;
        }
    }
}
