﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHurt : MonoBehaviour
{
    Animator anim;
    public delegate void Death();
    public static event Death OnBossDeath;

    BossScript bossScript;
    public int bossLives = 3;

    private void Start()
    {
        anim = GetComponentInParent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            if(bossLives > 0)
            {
                Debug.Log("Player Hit Me!");
                anim.Play("bossHurt");
                AudioManager.Play("bossHurtSound");
                Debug.Log("Current Lives: " + bossLives);
                bossLives--;
            }
            if(bossLives == 0)
            {
                Debug.Log("out of boss lives");
                OnBossDeath();
            }
        }
    }
}
