﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttack : MonoBehaviour
{
    Rigidbody2D body;

    public float speed = 3;
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
    }

    public void Attack(Vector3 dir, Vector3 pos)
    {
        gameObject.SetActive(true);
        if(body == null)
        {
            body = GetComponent<Rigidbody2D>();
        }
        transform.position = pos;
        body.velocity = (dir * speed);
        StartCoroutine(Life());
    }

    IEnumerator Life()
    {
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
        yield return new WaitForEndOfFrame();
    }
}
