﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemyController : MonoBehaviour
{
    public bool canFloat;
    public float duration;
    public int moveDistance;
    void Start()
    {
        StartCoroutine(EnemyMovement());
    }
    IEnumerator EnemyMovement()
    {
        Vector2 startPos = transform.position;

        if (canFloat) //flying enemy movement
        {
            Vector2 endPos = startPos + new Vector2 (0, moveDistance);
            while (enabled)
            {
                for (float t = 0; t < duration; t += Time.deltaTime)
                {
                    float frac = (t / duration);
                    frac = frac * frac / (2 * frac * frac - 2 * frac + 1); //Formula to ramp up the movement speed over time
                    Vector2.Lerp(startPos, endPos, frac);
                    yield return new WaitForEndOfFrame();
                }
                for (float t = 0; t < duration; t += Time.deltaTime)
                {
                    float frac = (t / duration);
                    frac = frac * frac / (2 * frac * frac - 2 * frac + 1);
                    Vector2.Lerp(endPos, startPos, frac);
                    yield return new WaitForEndOfFrame();
                }
            }
        }
        else //ground enemy movment
        {
            Vector2 endPos = startPos + new Vector2(moveDistance, 0);
            while(enabled)
            {
                for (float t = 0; t < duration; t += Time.deltaTime)
                {
                    float frac = (t / duration);
                    frac = frac * frac / (2 * frac * frac - 2 * frac + 1);
                    transform.position = Vector2.Lerp(startPos, endPos, frac);
                    yield return new WaitForEndOfFrame();
                }
                for (float t = 0; t < duration; t += Time.deltaTime)
                {
                    float frac = (t / duration);
                    frac = frac * frac / (2 * frac * frac - 2 * frac + 1);
                    transform.position = Vector2.Lerp(endPos, startPos, frac);
                    yield return new WaitForEndOfFrame();
                }
            }
        }
        yield return new WaitForEndOfFrame();
    }
}
