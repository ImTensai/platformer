﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public static PlayerController player;
    public LayerMask interactables = -1;
    CharacterMotor motor;
    public bool dead;
    
    void Awake()
    {
        if (player == null)
        {
            player = this;
            DontDestroyOnLoad(player);
        }
        else
        {
            Destroy(player);
        }
        motor = GetComponent<CharacterMotor>();
    }

    void Update()
    {
        motor.xInput = Input.GetAxisRaw("Horizontal");
        motor.shouldJump = (Input.GetKeyDown(KeyCode.Joystick1Button4)) || (Input.GetKeyDown("w"));
        if (Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown("e"))
        {
            Interact();
        }
    }

    void Interact()
    {
        Collider2D collider = Physics2D.OverlapCircle(transform.position, 0.1f, interactables);
        if (collider == null) return;
        
        InteractableController interactable = collider.gameObject.GetComponent<InteractableController>();
        if (interactable == null) return;

        interactable.Interact();
    }
}
