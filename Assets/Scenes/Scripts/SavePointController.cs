﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class SavePointController : InteractableController
{
    public static SavePointController save;

    Animator anim;

    public bool saved;

    [System.Serializable]
    public class SaveState
    {
        public string scene;
        public Vector3 position;
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        saved = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            Save();
            anim.Play("savedFlag");
        }
    }

    void Save()
    {
        SaveState saveState = new SaveState();
        saveState.scene = SceneManager.GetActiveScene().name;

        PlayerController player = PlayerController.player;
        saveState.position = player.transform.position;

        string path = Path.Combine(Application.persistentDataPath, "save.json");
        string json = JsonUtility.ToJson(saveState);

        File.WriteAllText(path, json);
        saved = true;
        Debug.Log(path);
    }

    public static bool SaveFileExists()
    {
        string path = Path.Combine(Application.persistentDataPath, "save.json");
        return File.Exists(path);
    }
    
    public static SaveState Load()
    {
        string path = Path.Combine(Application.persistentDataPath, "save.json");
        if (!File.Exists(path)) return null;
        
        string json = File.ReadAllText(path);
        return JsonUtility.FromJson<SaveState>(json);
    }

    public static bool DeleteSave()
    {
        string path = Path.Combine(Application.persistentDataPath, "save.json");
        if(!File.Exists(path))
        {
            Debug.Log("Save Does not exist");
            return false;
        }

        File.Delete(path);
        if (!File.Exists(path))
        {
            Debug.Log("Delete Successful");
            return true;
        }

        Debug.Log("Delete Failed");
        return false;
    }
}
    
