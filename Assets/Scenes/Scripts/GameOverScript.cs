﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    public static GameOverScript gameOver;

    public GameObject gameOverTextBox;
    public Text gameOverText;

    private void Awake()
    {
        if (gameOver == null)
        {
            gameOver = this;
        }
    }
    private void Start()
    {
        gameOverTextBox.SetActive(false);
    }
}
