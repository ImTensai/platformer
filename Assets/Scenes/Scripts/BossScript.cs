﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{
    Animator anim;

    Vector3 startPos;

    bool attacking;
    bool resetting;
    public bool bossDead;

    public float speed = 1.5f;
    public float chaseRadius = 10;
    public float minDist = 5;
    public float attackHeight = 0.5f;
    public float attackSpeed = 4;

    public ParticleSystem wingParticles_1;
    public ParticleSystem wingParticles_2;

    public GameObject leftGun;
    public GameObject rightGun;

    public GameObject projectilePrefab;

    GameObject leftProjectilePrefab;
    GameObject rightProjectilePrefab;

    Rigidbody2D body;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        startPos = transform.position;
        attacking = false;
        resetting = false;

        leftProjectilePrefab = Instantiate(projectilePrefab);
        leftProjectilePrefab.SetActive(false);
        rightProjectilePrefab = Instantiate(projectilePrefab);
        rightProjectilePrefab.SetActive(false);
    }

    private void OnEnable()
    {
        BossHurt.OnBossDeath += OnBossDeath;
    }

    private void OnDisable()
    {
        BossHurt.OnBossDeath -= OnBossDeath;
    }

    private void OnBossDeath()
    {
        StopAllCoroutines();
        StartCoroutine(Dead());
    }

    void Update()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(Vector2.zero, transform.position - PlayerController.player.transform.position);
        Gizmos.color = Color.blue;

        if (bossDead)
        {
            Debug.Log("Im ded");
            return;
        }

        if (Mathf.Abs(PlayerController.player.transform.position.x - transform.position.x) <= chaseRadius && Mathf.Abs(PlayerController.player.transform.position.x - transform.position.x) > minDist && !resetting)
        {
            Debug.DrawLine(transform.position, PlayerController.player.transform.position, Color.cyan);
            anim.Play("bossFlying");
            wingParticles_1.Play();
            wingParticles_2.Play();
            ChasePlayer();
            Debug.Log("Pursuing Player");
        }
        else if (Mathf.Abs(PlayerController.player.transform.position.x - transform.position.x) <= chaseRadius && Mathf.Abs(PlayerController.player.transform.position.x - transform.position.x) < minDist && !resetting && !attacking)
        {
            Debug.DrawLine(transform.position, PlayerController.player.transform.position, Color.red);
            wingParticles_1.Play();
            wingParticles_2.Play();
            transform.position = transform.position;
            attacking = true;
            Debug.Log("Getting into attack position");
            StartCoroutine(AttackPosition());
        }
        else if(!attacking && !resetting)
        {
            Debug.DrawLine(transform.position, PlayerController.player.transform.position, Color.green);
            anim.Play("bossFlying");
            transform.position = transform.position;
            Debug.Log("Player's not in range");
        }

        if (PlayerController.player.dead)
        {
            transform.position = startPos;
            anim.Play("bossFlying");
        }
    }

    void ChasePlayer()
    {
        transform.position = Vector3.MoveTowards(transform.position, PlayerController.player.transform.position, speed * Time.deltaTime);
    }

    IEnumerator AttackPosition()
    {
        Vector3 currentPos = transform.position;
        Vector3 attackPos = new Vector3(transform.position.x + 0, 0.5f, transform.position.z + 0);
        float duration = 2;

        Debug.DrawLine(transform.position, PlayerController.player.transform.position, Color.red);

        for (float i = 0; i < duration; i += Time.deltaTime)
        {
            transform.position = Vector3.Lerp(currentPos, attackPos,  i / duration);
            wingParticles_1.Play();
            wingParticles_2.Play();
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2);
        Debug.Log("In attack position");
        Attack();
    }

    void Attack()
    {
        if(leftProjectilePrefab.GetComponent<BossAttack>())
        {
            leftProjectilePrefab.GetComponent<BossAttack>().Attack(leftGun.transform.right, leftGun.transform.position);
        }

        if(rightProjectilePrefab.GetComponent<BossAttack>())
        {
            rightProjectilePrefab.GetComponent<BossAttack>().Attack(rightGun.transform.right, rightGun.transform.position);
        }

        attacking = false;
        resetting = true;
        StartCoroutine(Resetting());
    }

    IEnumerator Resetting()
    {
        if (bossDead)
        {
            yield return null;
        }
        yield return new WaitForSeconds(3);
        Vector3 attackPos = new Vector3(transform.position.x + 0, 0.5f, transform.position.z + 0);
        float duration = 2;

        for (float i = 0; i < 5; i += Time.deltaTime)
        {
            Debug.Log("Resetting");
            transform.position = Vector3.Lerp(attackPos, startPos, i / duration);
            wingParticles_1.Play();
            wingParticles_2.Play();
            yield return new WaitForEndOfFrame();
        }
        resetting = false;
        Debug.Log(gameObject.name + ": " + "Reset");
    }

    IEnumerator Dead()
    {
        bossDead = true;
        body.gravityScale = 1;
        body.constraints = RigidbodyConstraints2D.FreezePositionX;
        Debug.Log("Boss is dead");
        anim.Play("bossDeath");
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
        Application.Quit();
    }
}
