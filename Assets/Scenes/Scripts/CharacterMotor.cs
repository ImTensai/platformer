﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour
{
    Rigidbody2D body;

    Animator anim;

    public ParticleSystem dashEffect;

    public LayerMask envLayers;

    public float maxSpeedChange = 1;
    public float runSpeed = 3;

    public float jumpSpeed = 5;
    public float jumpWindow = 0.25f;
    public float lastGrounded = -1;
    public float jumpGravity = 2;
    public float fallGravity = 1;
    

    public float groundDist = 0.25f;
    
    public float dashSpeed = 2;
    private float dashTime;
    public float startDashTime = 2;
    

    public bool onGround = true;

    private float _xInput;
    public float xInput
    {
        get { return _xInput; }
        set { _xInput = value; }
    }

    private bool _shouldJump;
    public bool shouldJump
    {
        get { return _shouldJump; }
        set { _shouldJump = value; }
    }
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        Run();
        Duck();
        Jump();
        Dash();
    }

    public void Run()
    {
        // Player moves left and right

        if(xInput < .1f || xInput > .1f)
        {
            Vector2 velocity = body.velocity;
            Vector2 velocityChange = new Vector2(xInput * runSpeed, velocity.y) - velocity;

            if (velocityChange.sqrMagnitude > maxSpeedChange * maxSpeedChange)
            {
                velocityChange = velocityChange.normalized * maxSpeedChange;

            }
            body.AddForce(velocityChange * body.mass / Time.fixedDeltaTime);

            anim.SetFloat("speed", Mathf.Abs(velocity.x));

            // Player faces toward the direction their moving by rotating along the y-axis

            if (xInput > 0.1f)
            {
                transform.localEulerAngles = Vector3.zero;
            }
            else if (xInput < -0.1f)
            {
                transform.localEulerAngles = Vector3.up * 180f;
            }
        }
    }

    void Duck()
    {
        Vector2 velocity = body.velocity;
        if (Input.GetKeyDown("s"))
        {
            velocity.x = xInput * (runSpeed * 0.5f);
            //Debug.Log("Player ducking");
            //anim.SetTrigger("duckPress");
        }
        body.velocity = velocity;
    }

    void Jump()
    {
        // Player jumps when 'w' is pressed

        Vector2 velocity = body.velocity;

        if (onGround && shouldJump)
        {
            velocity.y = jumpSpeed;
            onGround = false;
            anim.SetTrigger("jumpPress");
            anim.SetBool("onGround", false);
            AudioManager.Play("2DjumpSound");
        }

        anim.SetFloat("yVel", velocity.y);
        body.velocity = velocity;
    }

    private void OnCollisionEnter2D(Collision2D c)
    {
        // Player onGround check after jumping

        for (int i = 0; i < c.contactCount; i += 1)
        {
            ContactPoint2D contactPoint = c.contacts[i];
            Vector2 norm = contactPoint.normal;

            if (Vector2.Angle(norm, Vector2.up) < 10)
            {
                onGround = true;
                anim.SetBool("onGround", true);
                AudioManager.Play("2DlandSound");
                return;
            }
        }
    }

    public void Dash()
    {
        // Player dashes then cooldown starts

        Vector2 velocity = body.velocity;
        if (dashTime <= 0 && (Input.GetKeyDown(KeyCode.JoystickButton5) || Input.GetKeyDown(KeyCode.Comma)))
        {
            velocity.x -= dashSpeed * 60 * Time.deltaTime;
            dashEffect.Play();
            anim.SetTrigger("dashPress");
            CameraController.Shake();
            dashTime = startDashTime;
        }
        if (dashTime <= 0 && (Input.GetKeyDown(KeyCode.JoystickButton6) || Input.GetKeyDown(KeyCode.Period)))
        {
            velocity.x += dashSpeed * 60 * Time.deltaTime;
            dashEffect.Play();
            anim.SetTrigger("dashPress");
            CameraController.Shake();
            dashTime = startDashTime;
        }

        dashTime -= Time.deltaTime;
        anim.SetFloat("xVel", velocity.x);
        body.velocity = velocity;
    }

    void GroundTest()
    {
        Collider2D[] env = Physics2D.OverlapCircleAll((Vector2)transform.position, groundDist, envLayers);
        onGround = env.Length > 0;

        if (onGround)
        {
            lastGrounded = Time.time;
            body.gravityScale = jumpGravity;
        }
        else if (body.velocity.y < 0)
        {
            body.gravityScale = fallGravity;
        }

        anim.SetBool("onGround", env.Length > 0);
    }
}
